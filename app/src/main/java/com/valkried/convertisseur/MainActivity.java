package com.valkried.convertisseur;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText inputValue = findViewById(R.id.inputValue);
        Button submit = findViewById(R.id.submit);
        final TextView displayInputValue = findViewById(R.id.displayInputValue);
        TextView displayOutputValue = findViewById(R.id.displayOutputValue);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayInputValue.setText(inputValue.getText() + " € =");
            }
        });
    }
}
